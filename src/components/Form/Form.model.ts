import { Size } from "services/scrollWowSvc";

export type TypeFaceOption = "classic" | "funky" | "fun";

export type FormModel = {
  gifText: string;
  textColor?: string;
  bgColor?: string;
  typeface?: TypeFaceOption;
  size?: Size;
};